﻿using System;
using System.Runtime.InteropServices;

namespace QRReader
{
    [Guid("82EEE63C-02DE-48A8-A6D4-2F6C0D40A050")]
    internal interface IQRReader
    {
        [DispId(1)]
        string text { get; }

        [DispId(2)]
        void start();
        [DispId(3)]
        void stop();
    }




    public class Reader: IQRReader
    {
        private QRReaderForm form;

        public string text
        {
            get {

                return form.result;
            }
        }

        public void start()
        {
            form = new QRReaderForm();
            form.Show();
        }

        public void stop()
        {
            form.Close();
            form = null;
        }

    }
}
