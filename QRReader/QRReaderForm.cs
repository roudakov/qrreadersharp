﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Media;
using System.Windows.Forms;
using AForge.Video;
using ZXing;
using AForge.Video.DirectShow;


namespace QRReader
{
    public partial class QRReaderForm : Form
    {
        public QRReaderForm()
        {
            InitializeComponent();
        }


        VideoCaptureDevice videoSource;
        BarcodeReader q;
        private string prev_result;
        public string result;

        private void QRReaderForm_Load(object sender, EventArgs e)
        {
            var videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (videoDevices.Count > 0)
            {
                initQRReader();
                videoSource = initCapture(videoDevices);
                videoSource.Start();
            }
            else
            {
                label1.Text = "No capture device";
            }


        }

        private VideoCaptureDevice initCapture(FilterInfoCollection videoDevices)
        {
            var videoSource = new VideoCaptureDevice(videoDevices[0].MonikerString);
            videoSource.NewFrame += new NewFrameEventHandler(video_NewFrame);

            videoSource.VideoResolution = videoSource.VideoCapabilities[0];
            foreach (var cap in videoSource.VideoCapabilities)
            {
                if (videoSource.VideoResolution.FrameSize.Width < cap.FrameSize.Width)
                    videoSource.VideoResolution = cap;
                label1.Text = videoSource.VideoResolution.FrameSize.ToString();
            }
            return videoSource;
        }

        private void doneCapture()
        {
            if (!(videoSource == null))
                if (videoSource.IsRunning)
                {
                    videoSource.SignalToStop();
                    videoSource = null;
                }
        }

        private void initQRReader()
        {
            q = new BarcodeReader();
            q.Options.PossibleFormats = new List<BarcodeFormat>();
            q.Options.PossibleFormats.Add(BarcodeFormat.QR_CODE);
            q.AutoRotate = true;
            q.Options.TryHarder = true;
        }

        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            var image = eventArgs.Frame;
            setImage((Bitmap)image.Clone());
            recognizeFrame((Bitmap)image.Clone());
        }


        private void setImage(Image img)
        {
            pictureBox1.Image = img;
        }


        public bool recognizeFrame(Bitmap image)
        {

            //label1.Text = image.Size.ToString();

            var barcodeResult = q.Decode(image);
            var curr_result = barcodeResult?.Text;

            if (!string.IsNullOrEmpty(curr_result))
            {
                if (curr_result == prev_result) //проверяем что это не повторно распознался тот же QR код
                {
                    result = "";
                }
                else
                {
                    prev_result = curr_result;
                    result = curr_result;
                    SystemSounds.Beep.Play();
                }
            }

            label1.Text = result;
            return !string.IsNullOrEmpty(result);
        }



        private void QRReaderForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            doneCapture();
        }

    }
}
